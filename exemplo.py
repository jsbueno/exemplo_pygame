import pygame


SIZE = 640, 480
CX, CY = SIZE[0] // 2, SIZE[1] // 2

def init():
    pygame.init()
    global SCREEN
    SCREEN = pygame.display.set_mode(SIZE)

def draw():
    sq_size = 40
    pygame.draw.rect(SCREEN, (255,0,0), (CX, CY, sq_size, sq_size))
    pygame.display.flip()

if __name__ == "__main__":
    try:
        init()
        draw()
        pygame.time.delay(3000)
    finally:
        pygame.quit()
